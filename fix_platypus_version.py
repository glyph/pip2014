
import os
import sys
import plistlib

with open("Pipify.platypus") as f:
    pl = plistlib.readPlist(f)

pl["Destination"] = os.path.abspath("./Pipify.app")
pl["Version"] = sys.argv[1]

with open("Pipify.platypus", "wb") as f:
    plistlib.writePlist(pl, f)
